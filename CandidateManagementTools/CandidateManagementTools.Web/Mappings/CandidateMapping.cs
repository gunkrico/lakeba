﻿using CandidateManagementTools.Entity;
using CandidateManagementTools.Web.Models;
using System.Collections.Generic;

namespace CandidateManagementTools.Web.Mappings
{
    public static class CandidateMapping
    {
        //mapping candidate model to candidate entity
        public static Candidate ToEntity(this CandidateModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new Candidate
            {
                Id = model.Id,
                Name = model.Name,
                Surname = model.Surname,
                Position = model.Position,
                CurriculumVitaePath = model.CurriculumVitaePath,
                IsActive = model.IsActive
            };
        }

        //mapping list of candidate model to list of candidate entity
        public static List<Candidate> ToEntities(List<CandidateModel> models)
        {
            if (models.Count <= 0)
            {
                return null;
            }

            var candidateEntities = new List<Candidate>();

            foreach (CandidateModel model in models)
            {
                var entity = ToEntity(model);
                candidateEntities.Add(entity);
            }

            return candidateEntities;
        }

        //mapping candidate entity to candidate model
        public static CandidateModel ToModel (Candidate entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new CandidateModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Surname = entity.Surname,
                Position = entity.Position,
                CurriculumVitaePath = entity.CurriculumVitaePath,
                IsActive = entity.IsActive
            };
        }

        //mapping list of candidate entity to list of candidate model
        public static List<CandidateModel> ToModels(List<Candidate> entities)
        {
            if (entities.Count <= 0)
            {
                return null;
            }

            var candidateModels = new List<CandidateModel>();

            foreach (Candidate entity in entities)
            {
                var model = ToModel(entity);
                candidateModels.Add(model);
            }

            return candidateModels;
        }
    }
}