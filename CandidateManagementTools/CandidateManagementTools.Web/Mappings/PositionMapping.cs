﻿using CandidateManagementTools.Entity;
using CandidateManagementTools.Web.Models;

namespace CandidateManagementTools.Web.Mappings
{
    public static class PositionMapping
    {
        //mapping candidate model to candidate entity
        public static Position ToEntity(this PositionModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new Position
            {
                Id = model.Id,                
                PositionName = model.Position,
                IsActive = model.IsActive
            };
        }

        //mapping candidate entity to candidate model
        public static PositionModel ToModel(Position entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new PositionModel
            {
                Id = entity.Id,
                Position = entity.PositionName,
                IsActive = entity.IsActive
            };
        }
    }
}