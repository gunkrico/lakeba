﻿using CandidateManagementTools.Entity;
using System;
using System.ComponentModel.DataAnnotations;

namespace CandidateManagementTools.Web.Models
{
    public class PositionModel
    {
        [Display(Name = "Position Id")]
        public int Id { get; set; }

        [Display(Name = "Position")]
        [Required(ErrorMessage = "Position is required")]
        public string Position { get; set; }


        public Boolean IsActive { get; set; }
     }
}