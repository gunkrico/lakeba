﻿using CandidateManagementTools.Data.Core;
using CandidateManagementTools.Data.Repository;
using CandidateManagementTools.Entity;
using CandidateManagementTools.Web.Mappings;
using CandidateManagementTools.Web.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace CandidateManagementTools.Web.Controllers
{
    [RoutePrefix("api/position")]
    public class PositionController : ApiController
    {
         private readonly IEntityRepository<Position> _positionRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PositionController(IEntityRepository<Position> positionRepository, IUnitOfWork unitOfWork)
        {
            _positionRepository = positionRepository;
            _unitOfWork = unitOfWork;
        }

        [Route]
        [HttpGet]
        public IEnumerable<PositionModel> GetAll()
        {
            var positions = _positionRepository.GetAll().ToArray().Select(PositionMapping.ToModel);
            return positions;
        }
    }
}
