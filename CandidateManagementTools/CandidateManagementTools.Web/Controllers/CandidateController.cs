﻿using CandidateManagementTools.Data.Core;
using CandidateManagementTools.Data.Repository;
using CandidateManagementTools.Entity;
using CandidateManagementTools.Web.Core;
using CandidateManagementTools.Web.Mappings;
using CandidateManagementTools.Web.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace CandidateManagementTools.Web.Controllers
{
    [RoutePrefix("api/candidate")]
    public class CandidateController : ApiController
    {
        private readonly IEntityRepository<Candidate> _candidateRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CandidateController(IEntityRepository<Candidate> candidateRepository, IUnitOfWork unitOfWork)
        {
            _candidateRepository = candidateRepository;
            _unitOfWork = unitOfWork;
        }

        [Route]
        [HttpGet]
        public IEnumerable<CandidateModel> GetAll()
        {
            var candidates = _candidateRepository.GetAll().ToArray().Select(CandidateMapping.ToModel);
            return candidates.Where(e => e.IsActive == true);
        }

        [Route("{id:int}")]
        [HttpGet]
        public CandidateModel GetById(int id)
        {
            var candidate = _candidateRepository.GetById(id);
            return CandidateMapping.ToModel(candidate);
        }

        [Route]
        [HttpPost]
        public IHttpActionResult Create(CandidateModel candidateModel)
        {
            var candidateEntity = CandidateMapping.ToEntity(candidateModel);

            //set status = active
            candidateEntity.IsActive = true;

            _candidateRepository.Create(candidateEntity);

            _unitOfWork.Commit();

            return Ok(candidateEntity);
        }

        [Route("{Id:int}")]
        [HttpPut]
        public IHttpActionResult Update(int id, CandidateModel candidateModel)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            var candidateEntity = _candidateRepository.GetById(id);

            if (candidateEntity == null) { return NotFound(); }

            //assign new value of candidate
            candidateEntity.Id = id;
            candidateEntity.Name = candidateModel.Name;
            candidateEntity.Surname = candidateModel.Surname;
            candidateEntity.Position = candidateModel.Position;
            candidateEntity.CurriculumVitaePath = candidateModel.CurriculumVitaePath;

            _candidateRepository.Update(candidateEntity);

            _unitOfWork.Commit();

            return Ok(candidateModel);
        }

        [Route("{Id:int}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var candidateEntity = _candidateRepository.GetById(id);

            if (candidateEntity == null) { return NotFound(); }

            //change status to inactive when delete data
            candidateEntity.IsActive = false;

            _candidateRepository.Update(candidateEntity);

            _unitOfWork.Commit();

            return Ok();
        }

        [MimeMultipart]
        [Route("cv/upload")]
        public async Task<IHttpActionResult> Post(HttpRequestMessage request, int id)
        {
            var candidateEntity = _candidateRepository.GetById(id);
            if (candidateEntity == null)
            {
                return NotFound();
            }
            else
            {
                var uploadPath = HttpContext.Current.Server.MapPath("~/CVCollections");

                var multipartFormDataStreamProvider = new UploadMultipartFormProvider(uploadPath);

                // Read the MIME multipart asynchronously 
                await Request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);


                string _localFileName = multipartFormDataStreamProvider.FileData
                    .Select(multiPartData => multiPartData.LocalFileName).FirstOrDefault();

                if (string.IsNullOrEmpty(_localFileName))
                {
                    return NotFound();
                }

                // update database
                candidateEntity.CurriculumVitaePath = Path.GetFileName(_localFileName);
                _candidateRepository.Update(candidateEntity);
                _unitOfWork.Commit();
            }

            return Ok();
        }
    }
}
