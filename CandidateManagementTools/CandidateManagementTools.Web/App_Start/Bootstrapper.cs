﻿using CandidateManagementTools.Web;
using System.Web.Http;

namespace TalentPool.Web
{
    public class Bootstrapper
    {
        public static void Run()
        {
            // Configure Autofac
            AutofacWebapiConfig.Initialize(GlobalConfiguration.Configuration);
        }
    }
}