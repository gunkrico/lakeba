﻿(function (app) {
    'use strict';

    app.directive('candidateForm', candidateForm)
       .directive('candidateList', candidateList);

    function candidateForm() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/scripts/spa/candidates/candidateForm.html'
        }
    }
    function candidateList() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/scripts/spa/candidates/candidateList.html'
        }
    }

})(angular.module('common.ui'));