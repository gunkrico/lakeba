﻿(function (app) {
    'use strict';

    app.controller('candidatesCtrl', candidatesCtrl);

    candidatesCtrl.$inject = ['$scope', '$location', '$rootScope', '$filter', 'apiService', 'notificationService', 'fileUploadService', 'quixxi'];
    function candidatesCtrl($scope, $location, $rootScope, $filter, apiService, notificationService, fileUploadService, quixxi) {
        quixxi.init();
        quixxi.capturePageView("manage candidate");

        $scope.searchText = '';
        var cvFile = null;
        $scope.prepareFiles = prepareFiles;
        $scope.displayForm = false;
        $scope.showForm = function () {
            $scope.newCandidate = {};
            cvFile = null;
            $scope.displayForm = true;
        }

        $scope.cancelForm = function () {
            $scope.displayForm = false;
        }

        function candidatesLoadCompleted(response) {
            $scope.candidates = response.data;
        }

        function candidatesLoadFailed(response) {
            notificationService.displayError('Failed!');
        }

        // load candidate from service
        function loadCandidates() {
            apiService.get('/api/candidate/', null,
            candidatesLoadCompleted,
            candidatesLoadFailed);
        }
        loadCandidates();

        //load available positions
        function loadAvailablePositions() {
            apiService.get('/api/position/', null,
            positionLoadCompleted,
            candidatesLoadFailed);
        }
        loadAvailablePositions();

        function positionLoadCompleted(response) {
            var selectedPosition = null;
            if ($scope.newCandidate) {
                selectedPosition = $filter('filter')(response.data, { Position: $scope.newCandidate.Position }, function (a, e) {
                    return a === e;
                })[0];
            } else {
                selectedPosition = response.data[0];
            }
            
            $scope.positions = {
                positions: response.data,
                selectedPosition: selectedPosition
            };
        }

        // add new candidate
        function addNewCandidate(newCandidate) {
            apiService.post('/api/candidate/', newCandidate,
            successAddNewCandidate,
            candidatesLoadFailed);
        }

        function successAddNewCandidate(response) {
            uploadCv(response.data);
            $scope.newCandidate = {};
        }

        // update candidate
        function updateCandidate(updatedCandidate) {
            apiService.put('/api/candidate/'+updatedCandidate.Id, updatedCandidate,
            successUpdateNewCandidate,
            candidatesLoadFailed);
        }

        function successUpdateNewCandidate(response) {
            uploadCv(response.data);
            loadCandidates();
            $scope.newCandidate = {};
        }

        // save candidate; will add new candidate if its not exist and update candidate if its exist
        $scope.saveCandidate = function () {
            quixxi.captureFormSubmitted($scope.newCandidate);
            $scope.newCandidate.Position = $scope.positions.selectedPosition.Position;
            if ($scope.newCandidate.Id == null) {
                addNewCandidate($scope.newCandidate);
            } else {
                updateCandidate($scope.newCandidate);
            }
            $scope.displayForm = false;
        };

        // delete candidate
        $scope.deleteCandidate = function (Id) {
            apiService.deleteCandidate('/api/candidate/' + Id, null,
            loadCandidates,
            candidatesLoadFailed);
        };

        // populate form with candidate to edit
        $scope.editCandidate = function (editCandidate) {
            $scope.newCandidate = editCandidate;
            $scope.displayForm = true;
            loadAvailablePositions();
        };

        //upload cv
        function uploadCv(candidate) {
            if (cvFile) {
                fileUploadService.uploadCV(cvFile, candidate.Id, loadCandidates);
            }
        }

        function prepareFiles($files) {
            cvFile = $files;
        }

    }

})(angular.module('candidateManagement'));