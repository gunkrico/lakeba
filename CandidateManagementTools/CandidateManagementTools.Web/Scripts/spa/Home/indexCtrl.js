﻿(function (app) {
    'use strict';

    app.controller('indexCtrl', indexCtrl);

    indexCtrl.$inject = ['$scope', '$location', '$rootScope', 'quixxi'];
    function indexCtrl($scope, $location, $rootScope, quixxi) {
        quixxi.init();
        quixxi.capturePageView('home page');
        $scope.pageClass = 'page-home';
    }

})(angular.module('candidateManagement'));