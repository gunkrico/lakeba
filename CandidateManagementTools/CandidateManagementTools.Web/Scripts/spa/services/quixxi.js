﻿(function (app) {
    'use strict';

    app.factory('quixxi', quixxi);

    function quixxi() {
        var appGuid = "380e5bf5-d8bc-47cd-8452-252c12a1d58f";
        var appKey = "I8Rja9AEjWLl5ZKtyosg";

        var service = {
            init: init,
            captureFormSubmitted: captureFormSubmitted,
            capturePageView: capturePageView
        }

        return service;

        function init() {
            Quixxi.init(appGuid, appKey, false);
        };

        function captureFormSubmitted(candidate) {
            Quixxi.q.push(['SendEvent', {
                "Event": "Save Submitted Candidate",
                "Details": candidate.Name + candidate.Surname,
                "Event_Detail": "Submit"
            }]);
        };

        function capturePageView(pageName) {
            Quixxi.q.push(['SendEvent', {
                "Event": "Page Viewed",
                "Details": pageName,
                "Event_Detail": "Page View"
            }]);
        };

    }
})(angular.module('common.core'));;