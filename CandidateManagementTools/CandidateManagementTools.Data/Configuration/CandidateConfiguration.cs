﻿using CandidateManagementTools.Entity;

namespace CandidateManagementTools.Data.Configuration
{
    public class CandidateConfiguration : EntityConfiguration<Candidate>
    {
        public CandidateConfiguration()
        {
            Property(c => c.Name).IsRequired().HasMaxLength(45);
            Property(c => c.Surname).IsOptional().HasMaxLength(45);
            Property(c => c.CurriculumVitaePath).IsOptional().HasMaxLength(500);
            Property(c => c.Position).IsOptional().HasMaxLength(45);
            Property(c => c.IsActive).IsOptional();
        }
    }
}
