﻿using CandidateManagementTools.Entity;
using System.Linq;

namespace CandidateManagementTools.Data.Repository
{
    public interface IEntityRepository<T> where T : class, IEntity, new()
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
