﻿using System;

namespace CandidateManagementTools.Data.Core
{
    public interface IDbFactory : IDisposable
    {
        CandidateManagementToolsContext Init();
    }
}
