﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using CandidateManagementTools.Entity;
using CandidateManagementTools.Data.Configuration;

namespace CandidateManagementTools.Data
{
    public class CandidateManagementToolsContext : DbContext
    {
        public CandidateManagementToolsContext()
            : base("lakeba")
        {
            Database.SetInitializer<CandidateManagementToolsContext>(null);
        }

        public IDbSet<Candidate> Candidates { get; set; }
        public IDbSet<Position> Positions { get; set; }        

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.Add(new CandidateConfiguration());
            modelBuilder.Configurations.Add(new PositionConfiguration());            
        }
    }
}
