﻿using System;

namespace CandidateManagementTools.Entity
{
    public interface IEntity
    {
        int Id { get; set; }
        // status for data has been deleted or not
        Boolean IsActive {get; set;}
    }
}
